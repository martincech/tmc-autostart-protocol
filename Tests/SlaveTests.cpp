#include <gmock/gmock.h>
#include "MockTimer.h"
#include "../Protocol/TMC-autostart-slave-wrapper.hpp"
#include "TMC-autostart.h"
#include <ostream>

using testing::_;
using testing::DoAll;
using testing::SaveArg;
using testing::NiceMock;
using testing::Return;

namespace TmcAutostartProtocol {
    namespace Receiver {

        class MockIoReader : public IoReader {
        public:
            MOCK_METHOD0(read, uint8_t());
            MOCK_METHOD1(receive, void(uint8_t));
        };

        struct SlaveTest : testing::Test {
            NiceMock<MockTimer> mockTimer;
            MockIoReader mockReader;

            Slave *uut = nullptr;
            TimerHandlerFnPtr timerCallback;
            ReceivedCallback receiveCallback;

            SlaveTest() {
                EXPECT_CALL(mockTimer, setTimer(_,_))
                    .Times(1)
                    .WillOnce(DoAll(SaveArg<1>(&timerCallback), Return(5)))
                    .RetiresOnSaturation();

                receiveCallback = [this](uint8_t value)
                {
                    mockReader.receive(value);
                };
                uut = new Slave(mockReader, mockTimer);
                uut->registerReceiveHandler(receiveCallback);
            }

            ~SlaveTest() {
                delete uut;
            }
        };

        struct Params {
            uint8_t expectedNumber;
            uint8_t framesCount;

            friend std::ostream &operator<<(std::ostream &os, const Params &obj) {
                return os
                       << "expectedNumber: " << (int)obj.expectedNumber
                       << " framesCount: " << (int)obj.framesCount;
            }
        };

        struct ParemetrizedSlaveTest : SlaveTest, testing::WithParamInterface<Params> {

            explicit ParemetrizedSlaveTest(){
            }
        };


        #define SimulateSingleState(state) \
            EXPECT_CALL(mockReader, read()) \
                .WillOnce(Return<uint8_t>(state)) \
                .RetiresOnSaturation()


        #define SimulateSilentPeriod(periodLength) \
            EXPECT_CALL(mockReader, read()) \
                .Times(periodLength) \
                .WillRepeatedly(Return<uint8_t>(DEASSERTED_STATE)) \
                .RetiresOnSaturation()

        #define SimulateSingleFrame() \
            testing::InSequence dummy1; \
            for (auto i = 0; i < expectedNumber; i++){  \
                testing::InSequence dummy2; \
                SimulateSingleState(ASSERTED_STATE); \
                SimulateSingleState(DEASSERTED_STATE); \
            } \
            SimulateSilentPeriod(SILENT_PERIOD)

        #define BaseExpectations() \
            const auto expectedNumber = GetParam().expectedNumber; \
            /* since read needs at least two valid frames to be received */ \
            const auto frames = GetParam().framesCount * 2; \
            EXPECT_CALL(mockReader, receive(_)) \
                .Times(0); \
            EXPECT_CALL(mockReader, receive(expectedNumber)) \
                .Times(expectedNumber == 0 ? 0 : 1)

                
        #define ExecuteTicks() \
            for (auto i = 0; i < ticks; i++) {\
                /*execute tick on UUT*/\
                timerCallback();\
            }

        #define TestStartInSilentPeriod(startupSilenceTicks) { \
            BaseExpectations(); \
            /* frame data period */ \
            auto ticks = expectedNumber * 2 \
            /* frame silence period */ \
                + SILENT_PERIOD;   \
            /* count of frames*/ \
            ticks *= frames;        \
            /* startup silence*/    \
            ticks += startupSilenceTicks;         \
            {\
                testing::InSequence dummy;\
                SimulateSilentPeriod(startupSilenceTicks);\
                for (auto c = 0; c < frames; c++) {\
                    SimulateSingleFrame();\
                }\
            }\
            ExecuteTicks(); \
        }

        #define TestStartInDataPeriod(periodOffset, firstState) { \
            BaseExpectations(); \
            /* frame data period */ \
            auto ticks = expectedNumber * 2 \
                /* frame silence period */ \
                + SILENT_PERIOD;   \
            /* count of frames*/ \
            ticks *= frames;        \
            /* startup data + silence */    \
            ticks += (firstState == DEASSERTED_STATE? 1 : 0) + ((uint8_t)periodOffset)*2 + SILENT_PERIOD;         \
            {\
                testing::InSequence dummy;\
                if(firstState == DEASSERTED_STATE){ \
                    SimulateSingleState(DEASSERTED_STATE); \
                }\
                for (auto c = 0; c < periodOffset; c++) {\
                    SimulateSingleState(ASSERTED_STATE);\
                    SimulateSingleState(DEASSERTED_STATE);\
                }\
                SimulateSilentPeriod(SILENT_PERIOD);\
                for (auto c = 0; c < frames; c++) {\
                    SimulateSingleFrame();\
                }\
            }\
            ExecuteTicks(); \
        }

        using StartOnSilentPeriodBegining = ParemetrizedSlaveTest;
        TEST_P(StartOnSilentPeriodBegining, StartOnSilentPeriodBegining) {
            TestStartInSilentPeriod(SILENT_PERIOD);
        }

        using StartOnSilentPeriodEnd = ParemetrizedSlaveTest;
        TEST_P(StartOnSilentPeriodEnd, StartOnSilentPeriodEnd) {
            TestStartInSilentPeriod(1);
        }

        using StartInTheMiddleOfSilentPeriod = ParemetrizedSlaveTest;
        TEST_P(StartInTheMiddleOfSilentPeriod, StartInTheMiddleOfSilentPeriod) {
            TestStartInSilentPeriod(4);
        }

        using StartOn1InTheMiddleOfFrame = ParemetrizedSlaveTest;
        TEST_P(StartOn1InTheMiddleOfFrame, StartOn1InTheMiddleOfFrame) {
            TestStartInDataPeriod(GetParam().expectedNumber/2, ASSERTED_STATE);
        }

        using StartOn0InTheMiddleOfFrame = ParemetrizedSlaveTest;
        TEST_P(StartOn0InTheMiddleOfFrame, StartOn0InTheMiddleOfFrame) {
            TestStartInDataPeriod(GetParam().expectedNumber/2, DEASSERTED_STATE);
        }

        using StartOn1AtTheEndOfFrame = ParemetrizedSlaveTest;
        TEST_P(StartOn1AtTheEndOfFrame, StartOn1AtTheEndOfFrame) {
            TestStartInDataPeriod(1, ASSERTED_STATE);
        }

        using StartOn0AtTheEndOfFrame = ParemetrizedSlaveTest;
        TEST_P(StartOn0AtTheEndOfFrame, StartOn0AtTheEndOfFrame) {
            TestStartInDataPeriod(0, DEASSERTED_STATE);
        }

#define INSTANTIATE_TEST_CASES_FOR(TST) \
            INSTANTIATE_TEST_CASE_P( TST, TST, \
                testing::Values(    \
                    Params{3, 1},   \
                    Params{3, 2},   \
                    Params{3, 3},   \
                    Params{2, 1},   \
                    Params{2, 2},   \
                    Params{2, 3},   \
                    Params{0, 1},   \
                    Params{0, 2},   \
                    Params{0, 3},   \
                    Params{1, 1},   \
                    Params{1, 2},   \
                    Params{1, 3},   \
                    Params{10, 1},  \
                    Params{10, 2},  \
                    Params{10, 3}   \
                )                   \
            )

        INSTANTIATE_TEST_CASES_FOR(StartOnSilentPeriodBegining);
        INSTANTIATE_TEST_CASES_FOR(StartInTheMiddleOfSilentPeriod);
        INSTANTIATE_TEST_CASES_FOR(StartOnSilentPeriodEnd);

        INSTANTIATE_TEST_CASES_FOR(StartOn1InTheMiddleOfFrame);
        INSTANTIATE_TEST_CASES_FOR(StartOn0InTheMiddleOfFrame);
        INSTANTIATE_TEST_CASES_FOR(StartOn1AtTheEndOfFrame);
        INSTANTIATE_TEST_CASES_FOR(StartOn0AtTheEndOfFrame);
    }
}
