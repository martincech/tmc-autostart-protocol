#pragma once
#include "../Protocol/Timer.h"
#include <gmock/gmock.h>

namespace TmcAutostartProtocol {
    class MockTimer : public Timer {
    public:
        MOCK_METHOD2(setTimer, uint32_t(std::chrono::milliseconds, TimerHandlerFnPtr));
        MOCK_METHOD1(cancelTimer, void(uint32_t));
    };
}  // namespace TmcAutostartProtocol
