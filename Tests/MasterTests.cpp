#include <gmock/gmock.h>
#include "MockTimer.h"
#include "../Protocol/TMC-autostart-master-wrapper.hpp"
#include "TMC-autostart.h"

using testing::_;
using testing::DoAll;
using testing::Return;
using testing::SaveArg;
using testing::NiceMock;

namespace TmcAutostartProtocol {
    namespace Sender {

        class MockIoWriter : public IoWriter {
        public:
            MOCK_METHOD1(write, void(uint8_t));
        };

        struct MasterTest : testing::Test {
            NiceMock<MockTimer> mockTimer;
            MockIoWriter mockWriter;

            Master *uut = nullptr;
            TimerHandlerFnPtr timerCallback;

            MasterTest() {
                EXPECT_CALL(mockTimer, setTimer(_,_))
                    .Times(1)
                    .WillOnce(DoAll(SaveArg<1>(&timerCallback), Return(5)));
                uut = new Master(mockWriter, mockTimer);
            }

            ~MasterTest() {
                delete uut;
            }
        };

        struct ParametrizedMasterTest : MasterTest, testing::WithParamInterface<int> {

            explicit ParametrizedMasterTest() {
            }
        };


        TEST_P(ParametrizedMasterTest, ParametrizedMasterTest) {
            const auto numberToSend = GetParam();
            EXPECT_CALL(mockWriter, write(ASSERTED_STATE))
                .Times(numberToSend * 2);
            EXPECT_CALL(mockWriter, write(DEASSERTED_STATE))
                .Times(numberToSend * 2 + SILENT_PERIOD + SILENT_PERIOD);

            uut->send(numberToSend);
            const auto ticks = SILENT_PERIOD   /* starting silence*/
                               + numberToSend * 2  /*first period */
                               + SILENT_PERIOD       /* silent between frames*/
                               + numberToSend * 2; /*second period */
            for (auto i = 0; i < ticks; i++) {
                timerCallback();
            }
        }

        INSTANTIATE_TEST_CASE_P(SendingNumbers, ParametrizedMasterTest,
            testing::Values(
                3,   /* Odd*/
                2,   /* Even*/
                0,   /* Zero case*/
                1,   /* one case*/
                10,  /* random*/
                25,  /* random*/
                55,  /* random*/
                115, /* random*/
                255  /* max number*/
            )
        );

        TEST_F(MasterTest, OneSilentPeriodsOnBeginning) {
            const auto n = 5;
            {
                testing::InSequence dummy;
                // expect in sequence calls
                EXPECT_CALL(mockWriter, write(DEASSERTED_STATE))
                    .Times(SILENT_PERIOD)
                    .RetiresOnSaturation();
                EXPECT_CALL(mockWriter, write(ASSERTED_STATE)).RetiresOnSaturation();
                EXPECT_CALL(mockWriter, write(DEASSERTED_STATE)).RetiresOnSaturation();
                EXPECT_CALL(mockWriter, write(ASSERTED_STATE)).RetiresOnSaturation();
                EXPECT_CALL(mockWriter, write(DEASSERTED_STATE)).RetiresOnSaturation();
                EXPECT_CALL(mockWriter, write(ASSERTED_STATE)).RetiresOnSaturation();
                EXPECT_CALL(mockWriter, write(DEASSERTED_STATE)).RetiresOnSaturation();
                EXPECT_CALL(mockWriter, write(ASSERTED_STATE)).RetiresOnSaturation();
                EXPECT_CALL(mockWriter, write(DEASSERTED_STATE)).RetiresOnSaturation();
                EXPECT_CALL(mockWriter, write(ASSERTED_STATE)).RetiresOnSaturation();
                EXPECT_CALL(mockWriter, write(DEASSERTED_STATE)).RetiresOnSaturation();
            }
            uut->send(n);
            for (auto i = 0; i < n * 2 + SILENT_PERIOD; i++) {
                timerCallback();
            }
        }

    }
}
