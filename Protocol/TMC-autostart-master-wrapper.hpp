#ifndef __TMC_AUTOSTART_MASTER_WRAPPER_HPP__
#define __TMC_AUTOSTART_MASTER_WRAPPER_HPP__
#include "Timer.h"

namespace TmcAutostartProtocol {
    namespace Sender {
        class IoWriter {
        public:
            virtual ~IoWriter() = default;
            virtual void write(uint8_t on) = 0;
        };

        class Master {
            TimerHandlerFnPtr timerHandleFn;
        public:
            Master(IoWriter &io, Timer &timer);
            void send(uint8_t val) const;
        };
    }
}

#endif
