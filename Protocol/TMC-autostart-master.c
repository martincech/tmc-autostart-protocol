#define TMC_AUTOSTART_MASTER
#include "TMC-autostart.h"
#include "TMC-autostart-internal.h"
#ifdef __cplusplus
extern "C" {
#endif

extern Status status;
extern uint8_t transitionsOnLineCounter;
extern uint8_t valueCountedOnLine;
static OutputWriteFn outputFn;


void TmcAutostartMasterInit(const OutputWriteFn changeOutputFn) {
    InitFirstFrame();
    outputFn = changeOutputFn;
}

void TmcAutostartSend(uint8_t value) {
    if (valueCountedOnLine == value) {
        return;
    }
    valueCountedOnLine = value;
}

void TmcAutostartMasterExecute() {
    if (IsSendingSilentPeriod()) {
        status.lineState = DEASSERTED_STATE;
        status.silentPeriodCounter--;
        if (IsSilentPeriodOver()) {
            StartSendingFrame();
        }
    } else {
        /* sending frame */
        if (IsSendingFrame()) {
            if (status.lineState == DEASSERTED_STATE) {
                status.lineState = ASSERTED_STATE;
            } else {
                status.lineState = DEASSERTED_STATE;
                transitionsOnLineCounter--;
            }
        }
        if (IsFrameOver()) {
            StartSilentPeriod();
        }
    }

    outputFn(status.lineState);
}


#ifdef __cplusplus
}
#endif
