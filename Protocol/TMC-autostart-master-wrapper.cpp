#include "TMC-autostart-master-wrapper.hpp"
#define TMC_AUTOSTART_MASTER
#include "TMC-autostart.h"
#include <chrono>

using namespace std::chrono;

namespace TmcAutostartProtocol {
    namespace Sender {
        static IoWriter *ioW;

        void write(uint8_t on) {
            if (ioW != nullptr) {
                ioW->write(on);
            }
        }

        Master::Master(IoWriter &io, Timer &timer) {

            timerHandleFn = []()
            {
                TmcAutostartMasterExecute();
            };
            ioW = &io;
            TmcAutostartMasterInit(write);

            timer.setTimer(1s, timerHandleFn);
        }

        void Master::send(uint8_t val) const {
            TmcAutostartSend(val);
        }
    } // namespace Sender
}     // namespace TmcAutostartProtocol
