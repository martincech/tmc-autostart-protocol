#include "TMC-autostart.h"
#include "TMC-autostart-internal.h"

Status status;
uint8_t transitionsOnLineCounter = 0;
uint8_t valueCountedOnLine = 0;


void InitFirstFrame() {
    status.lineState = DEASSERTED_STATE;
    status.previousLineState = DEASSERTED_STATE;
    StartSilentPeriod();
    transitionsOnLineCounter = valueCountedOnLine = 0;
}

void StartSilentPeriod() {
    status.silentPeriodCounter = SILENT_PERIOD;
}

uint8_t IsSendingSilentPeriod() {
    return status.silentPeriodCounter != 0;
}

uint8_t IsSilentPeriodOver() {
    return !IsSendingSilentPeriod();
}

void StartSendingFrame() {
    transitionsOnLineCounter = valueCountedOnLine;
}

uint8_t IsSendingFrame() {
    return transitionsOnLineCounter != 0;
}

uint8_t IsFrameOver() {
    return !IsSendingFrame();
}
