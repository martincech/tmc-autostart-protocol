#define TMC_AUTOSTART_SLAVE
#include "TMC-autostart.h"
#include "TMC-autostart-internal.h"
#ifdef __cplusplus
extern "C" {
#endif

extern Status status;
extern uint8_t transitionsOnLineCounter;
extern uint8_t valueCountedOnLine;
static uint8_t previousValueCountedOnLine;
static InputReadFn inputFn;

static uint8_t Transition_0_0();
static uint8_t Transition_1_0();
static uint8_t Transition_0_1();
static uint8_t SameValueOnLineForTwoPeriods();

void TmcAutostartSlaveInit(const InputReadFn readInputFn, uint8_t storedValueOnLine) {
    InitFirstFrame();
    previousValueCountedOnLine = 0;
    valueCountedOnLine = storedValueOnLine;
    inputFn = readInputFn;
}

uint8_t TmcAutostartSlaveExecute() {
    status.previousLineState = status.lineState;
    status.lineState = inputFn();

    if (Transition_1_0() || Transition_0_1()) {
        StartSilentPeriod();
    }
    if (Transition_0_0()) {
        status.silentPeriodCounter--;
        if (IsSilentPeriodOver()) {
            if (SameValueOnLineForTwoPeriods()) {
                valueCountedOnLine = previousValueCountedOnLine;
            }
            StartSilentPeriod();
            previousValueCountedOnLine = transitionsOnLineCounter;
            transitionsOnLineCounter = 0;
        }
    }
    if (Transition_1_0()) {
        transitionsOnLineCounter++;
    }
    return valueCountedOnLine;
}

static uint8_t SameValueOnLineForTwoPeriods() {
    return transitionsOnLineCounter == previousValueCountedOnLine;
}

/* Transtion from DEASSERTED_STATE to DEASSERTED_STATE line state*/
static uint8_t Transition_0_0() {
    return status.previousLineState == DEASSERTED_STATE && status.lineState == DEASSERTED_STATE;
}

/* Transtion from ASSERTED_STATE to DEASSERTED_STATE line state*/
static uint8_t Transition_1_0() {
    return status.previousLineState == ASSERTED_STATE && status.lineState == DEASSERTED_STATE;
}

/* Transtion from DEASSERTED_STATE to ASSERTED_STATE line state*/
static uint8_t Transition_0_1() {
    return status.previousLineState == DEASSERTED_STATE && status.lineState == ASSERTED_STATE;
}


#ifdef __cplusplus
}
#endif
