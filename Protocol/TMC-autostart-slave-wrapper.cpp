#include "TMC-autostart-slave-wrapper.hpp"
#define TMC_AUTOSTART_SLAVE
#include "TMC-autostart.h"
#include <chrono>

using namespace std::chrono;

namespace TmcAutostartProtocol {
    namespace Receiver {
        static IoReader *ioR;

        uint8_t read() {
            if (ioR != nullptr) {
                return ioR->read();
            }
            return 0;
        }

        Slave::Slave(IoReader &io, Timer &timer, uint8_t defaultValue)
            : value(defaultValue)
            , lastValue(0) {
            timerHandleFn = [this]()
            {
                value = TmcAutostartSlaveExecute();
                if (lastValue != value) {
                    lastValue = value;
                    executeCallbackHandlers(value);
                }
            };
            ioR = &io;
            TmcAutostartSlaveInit(read, value);
            timer.setTimer(1s, timerHandleFn);
        }
    } // namespace Receiver
}     // namespace TmcAutostartProtocol
