#ifndef __TMC_AUTOSTART_PROTO_INTERNAL_H__
#define __TMC_AUTOSTART_PROTO_INTERNAL_H__
#include <stdint.h>

#ifdef __cplusplus
extern "C"{
#endif
    typedef struct {
        uint8_t lineState : 1;
        uint8_t previousLineState : 1;
        uint8_t silentPeriodCounter : 3;
    } Status;

    void InitFirstFrame();

    void StartSilentPeriod();
    uint8_t IsSendingSilentPeriod();
    uint8_t IsSilentPeriodOver();

    void StartSendingFrame();
    uint8_t IsSendingFrame();
    uint8_t IsFrameOver();

#ifdef __cplusplus
}
#endif

#endif  // __TMC_AUTOSTART_PROTO_INTERNAL_H__
