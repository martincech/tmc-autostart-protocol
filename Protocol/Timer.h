#ifndef __TMC_AUTOSTART_PROTOCOL_TIMER_H__
#define __TMC_AUTOSTART_PROTOCOL_TIMER_H__

#include <cstdint>
#include <chrono>
#include <random>
#include <functional>


namespace TmcAutostartProtocol {

    using TimerHandlerFnPtr = std::function<void()>;

    class Timer {
    public:
        virtual ~Timer() = default;

        /**
         * \brief Sets a timer to call handler function in duration milliseconds.
         * \param duration when to call the handler
         * \param handler handler to call
         * \return UID of the running timer to be able to cancel it. Returns 0 if the timer has not been set
         */
        virtual uint32_t setTimer(std::chrono::milliseconds duration, TimerHandlerFnPtr handler) = 0;
        /**
         * \brief Cancels the previously set timer
         * \param uid running timer UID as returned from setTimer
         */
        virtual void cancelTimer(uint32_t uid) = 0;
    };

} // namespace J1939
#endif  // __TMC_AUTOSTART_PROTOCOL_TIMER_H__
