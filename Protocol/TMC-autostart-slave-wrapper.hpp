#ifndef __TMC_AUTOSTART_SLAVE_WRAPPER_HPP__
#define __TMC_AUTOSTART_SLAVE_WRAPPER_HPP__
#include "Timer.h"

namespace TmcAutostartProtocol {
    namespace Receiver {
        class IoReader {
        public:
            virtual uint8_t read() = 0;
        };

        using ReceivedCallback = std::function<void(uint8_t)>;

        class Slave {
            std::vector<ReceivedCallback> callbacks;
            TimerHandlerFnPtr timerHandleFn;
            uint8_t value;
            uint8_t lastValue;

            void executeCallbackHandlers(uint8_t value) {
                for (const auto &callback : callbacks) {
                    callback(value);
                }
            }

        public:
            Slave(IoReader &io, Timer &timer, uint8_t defaultValue=0);

            virtual void registerReceiveHandler(ReceivedCallback &handler) {
                callbacks.push_back(handler);
            }
        };

    }
}

#endif
