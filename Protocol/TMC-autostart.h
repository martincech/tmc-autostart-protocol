#ifndef __TMC_AUTOSTART_PROTO_H__
#define __TMC_AUTOSTART_PROTO_H__
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif
typedef enum ELineState {
    DEASSERTED_STATE = 0x01,
    ASSERTED_STATE = 0x00
} LineState;

#define SILENT_PERIOD 7

#ifdef TMC_AUTOSTART_MASTER
    typedef void (*OutputWriteFn)(uint8_t on);
    void TmcAutostartMasterInit(const OutputWriteFn changeOutputFn);
    void TmcAutostartMasterExecute();
    void TmcAutostartSend(uint8_t value);
#endif

#ifdef TMC_AUTOSTART_SLAVE
    typedef uint8_t (*InputReadFn)();
    void TmcAutostartSlaveInit(const InputReadFn readInputFn, uint8_t storedValueOnLine);
    uint8_t TmcAutostartSlaveExecute();
#endif


#ifdef __cplusplus
}
#endif

#endif  // __TMC_AUTOSTART_PROTO_H__
