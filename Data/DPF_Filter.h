#ifndef __DPF_FILTER_H__
#define __DPF_FILTER_H__
    typedef enum {
        DPF_NORMAL_OPERATION,
        // STANDSTILL is running
        DPF_STANDSTILL_ONGOING,
        // STANDSTILL required - 50 hours until next level
        DPF_STANDSTILL_REQUIRED,
        // STANDSTILL required warning level - 25 hours untill next level
        DPF_STANDSTILL_REQUIRED_WARNING,
        // STANDSTILL required - service tool needed     
        DPF_STANDSTILL_REQUIRED_SERVICE,
        // STANDSTILL required but not possible - DPF filter CHANGE required 
        DPF_SERVICE_REQUIRED_FILTER_REMOVAL,             
        // OIL CHANGE required         
        DPF_SERVICE_REQUIRED_OIL,
        // DPF filter CHANGE required 
        DPF_SERVICE_REQUIRED_FILTER_WASH,        
        // DPF system failure
        DPF_STANDSTILL_ERROR,                    
        _DPF_STATE_COUNT
    } TDpfState;

#endif